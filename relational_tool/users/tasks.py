from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.utils import timezone, translation
from django.utils.translation import gettext as _
from django.utils.text import format_lazy
import requests
from relational_tool.users.models import User

from config import celery_app

message_template = _("""Hallo {user.first_name} {user.last_name}

Klicke auf diesen Link um dich anzumelden:

{login_url}

Dieser Link meldet dich automatisch an, leite ihn deshalb auf keinen Fall weiter!

Freundliche Grüsse!
""")


@celery_app.task
def send_auth_link(email, login_url):
    """Sends an auth link to the user."""
    user = User.objects.filter(email=email, is_active=True).first()

    translation.activate(user.language)

    send_mail(
        subject=settings.EMAIL_SUBJECT_PREFIX + " " + _("Anmeldelink"),
        message=format_lazy(message_template, login_url=login_url, user=user),
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[user.email],
        fail_silently=False,
    )


welcome_message_template = _("""Hallo {user.first_name} {user.last_name}

Willkommen bei der Kampagne für die 99%!
Klicke auf diesen Link um dich anzumelden:

{login_url}

Dieser Link meldet dich automatisch an, leite ihn deshalb auf keinen Fall weiter!
Falls du Fragen zur Initiative hast, findest du alle relevanten Informationen unter https://99prozent.ch.

Liebe Grüsse,
Dein Kampagnenteam""")


@celery_app.task
def send_success_message(user_pk, auth_link):
    user = User.objects.get(pk=user_pk)
    translation.activate(user.language)

    send_mail(
        subject=format_lazy(_("Herzlich willkommen {user.first_name}!"), user=user),
        message=format_lazy(welcome_message_template, user=user, login_url=auth_link),
        from_email=settings.DEFAULT_FROM_EMAIL,
        recipient_list=[user.email],
        fail_silently=False,
    )


@celery_app.task
def new_user(pk):
    user = User.objects.filter(pk=pk).first()
    if user is None:
        print(pk)
        print(User.objects.all().count())

    data = {
        'first_name': user.first_name,
        'last_name': user.last_name,
        'city': user.city,
        'phone': user.phone,
        'email': user.email,
        'language': user.language,
        'timestamp': timezone.now().isoformat(),
        'organizer_email': user.organizer_email,
    }

    print(settings.NEW_USER_HOOK_URL)

    requests.post(settings.NEW_USER_HOOK_URL,
                  json=data,
                  auth=(settings.NEW_USER_HOOK_USER, settings.NEW_USER_HOOK_AUTH))
