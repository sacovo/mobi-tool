from urllib.parse import parse_qs, urlencode, urlparse, urlunparse
import uuid

from django.contrib.auth import get_user_model, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.http.request import HttpRequest
from django.shortcuts import redirect, render
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.views.generic import DetailView, RedirectView, UpdateView
from sesame.utils import get_parameters, get_query_string

from relational_tool.users.forms import (
    AccountDeletionForm,
    AccountForm,
    LoginForm,
    SignupForm,
)
from relational_tool.users.tasks import send_auth_link, send_success_message

User = get_user_model()


def get_auth_link(request, user, url):
    sesam_params = get_parameters(user)
    scheme, netloc, path, params, query, fragment = urlparse(url or '')
    path = path or '/'
    next_params = parse_qs(query)
    next_params.update(sesam_params)
    new_query = urlencode(next_params, doseq=True)
    return request.build_absolute_uri(urlunparse((scheme, netloc, path, params, new_query, fragment)))


def login_view(request: HttpRequest):
    form = LoginForm()
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            user = User.objects.filter(email=email, is_active=True).first()
            login_url = get_auth_link(request, user, request.GET.get('next'))
            send_auth_link.delay(email, login_url)
            return render(request, 'users/login_link_sent.html', {'user': user})

    return render(request, 'users/login_view.html', {'form': form})


def signup_view(request):
    form = SignupForm()

    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.instance
            auth_link = get_auth_link(request, user, '/create/')
            send_success_message.delay(user.pk, auth_link)

            return render(request, "pages/signup_success.html", {'form': form})

    return render(request, 'users/signup_view.html', {'form': form})


@login_required
def logout_view(request):
    user = request.user
    logout(request)
    return render(request, "users/logout_success.html", {'user': user})


@login_required
def account_view(request):
    return render(request, 'users/account_view.html')


@login_required
def account_update(request):
    form = AccountForm(instance=request.user)
    if request.method == "POST":
        form = AccountForm(request.POST, instance=request.user)

        if form.is_valid():
            form.save()
            return redirect('users:account')
    return render(request, 'users/account_update.html', {'form': form})


@login_required
def account_delete(request):

    form = AccountDeletionForm()

    if request.method == "POST":
        form = AccountDeletionForm(request.POST)

        if form.is_valid():
            user = request.user
            logout(request)
            user.is_active = False
            user.username = str(uuid.uuid4())
            user.save()
            return redirect('home')

    return render(request, 'users/account_delete.html', {'form': form})
