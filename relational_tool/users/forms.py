from django import forms
from django.contrib.auth import forms as admin_forms
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

User = get_user_model()


class UserChangeForm(admin_forms.UserChangeForm):
    class Meta(admin_forms.UserChangeForm.Meta):
        model = User


class AccountForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'zip_code', 'city', 'phone', 'language']


class AccountDeletionForm(forms.Form):
    confirm = forms.BooleanField(required=True, label=_("Ich möchte meinen Account löschen"))


class LoginForm(forms.Form):
    email = forms.EmailField(label=_("email address"))

    def clean_email(self):
        email = self.cleaned_data['email']
        if get_user_model().objects.filter(email=email, is_active=True).first() is None:
            raise ValidationError(_("Kein Benutzer mit dieser E-Mail Adresse."))
        return email


class SignupForm(forms.ModelForm):
    def save(self):
        self.instance.username = self.instance.email
        super().save()

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'phone', 'zip_code', 'city', 'language']

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(username=email).exists():
            raise ValidationError(_("Diese E-Mail Adresse wurde bereits registriert"))
        return email


class PageSignupForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        self.page = kwargs.pop('page', None)
        super().__init__(*args, **kwargs)
        for field in self.fields.items():
            field[1].required = True

    def save(self):
        self.instance.username = self.instance.email
        if self.page is not None:
            self.instance.language = self.page.owner.language
            self.instance.signed_up_through = self.page
        super().save()

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'email', 'phone', 'zip_code', 'city']

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(username=email).exists():
            raise ValidationError(_("Diese E-Mail Adresse wurde bereits registriert"))
        return email


class UserCreationForm(admin_forms.UserCreationForm):
    class Meta(admin_forms.UserCreationForm.Meta):
        model = User

        error_messages = {"username": {"unique": _("This username has already been taken.")}}
