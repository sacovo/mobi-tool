from typing import Iterable, Optional
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse
from django.utils.translation import get_language_info, gettext_lazy as _


class User(AbstractUser):
    """Default user for Relational Tool."""

    zip_code = models.CharField(max_length=10, blank=True, verbose_name=_("PLZ"))
    city = models.CharField(max_length=100, blank=True, verbose_name=_("Ort"))
    phone = models.CharField(max_length=30, blank=True, verbose_name=_("Telefon"))
    email = models.EmailField(verbose_name=_("email address"))
    signed_up_through = models.ForeignKey(
        "pages.Page",
        models.SET_NULL,
        blank=True,
        null=True,
        related_name="contacts",
    )
    language = models.CharField(choices=sorted([(lang[0], get_language_info(lang[0])['name_local'])
                                                for lang in settings.LANGUAGES],
                                               key=lambda x: x[1]),
                                default=settings.LANGUAGE_CODE,
                                max_length=30,
                                verbose_name=_("Sprache"))

    def get_absolute_url(self):
        """Get url for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"username": self.username})

    def save(self,
             force_insert: bool = False,
             force_update: bool = False,
             using: Optional[str] = None,
             update_fields: Optional[Iterable[str]] = None) -> None:
        if self.pk is None:
            from relational_tool.users import tasks
            ret_val = super().save(force_insert=force_insert,
                                   force_update=force_update,
                                   using=using,
                                   update_fields=update_fields)
            tasks.new_user.delay(self.pk)
            return ret_val
        return super().save(force_insert=force_insert,
                            force_update=force_update,
                            using=using,
                            update_fields=update_fields)

    @property
    def organizer_email(self):
        if self.signed_up_through is not None:
            return self.signed_up_through.owner.email
        return ''
