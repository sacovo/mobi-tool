from django.urls import path

from relational_tool.users.views import (
    account_update,
    account_delete,
    account_view,
    login_view,
    logout_view,
    signup_view,
)

app_name = "users"
urlpatterns = [
    path("login/", view=login_view, name="account_login"),
    path("account/", view=account_view, name="account"),
    path("account/update/", view=account_update, name="account_update"),
    path("account/delete/", view=account_delete, name="account_delete"),
    path("signup/", view=signup_view, name="account_signup"),
    path("account/logout/", view=logout_view, name="account_logout"),
]
