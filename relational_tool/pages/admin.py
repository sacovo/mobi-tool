from relational_tool.pages.models import Page
from django.contrib import admin

# Register your models here.


@admin.register(Page)
class PageAdmin(admin.ModelAdmin):
    pass
