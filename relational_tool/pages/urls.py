from django.urls.conf import path
from django.views.generic.base import TemplateView
from relational_tool.pages.views import PageCreateView, PageDetailView, PageUpdateView, contacts_csv, contacts_view

app_name = 'pages'

urlpatterns = [
    path("create/", PageCreateView.as_view(), name="page_create"),
    path("<uuid:pk>/", PageDetailView.as_view(), name="page_detail"),
    path("<uuid:pk>/update/", PageUpdateView.as_view(), name="page_update"),
    path("<uuid:pk>/share/", PageUpdateView.as_view(), name="page_share"),
    path("contacts/", contacts_view, name="contacts"),
    path("contacts/csv/", contacts_csv, name="contacts_csv"),
    path("success/", TemplateView.as_view(template_name="pages/signup_success.html"), name="success_page"),
]
