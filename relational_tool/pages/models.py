import uuid
from django.contrib.auth import get_user_model
from django.db import models
from django.urls.base import reverse
from model_utils.models import TimeStampedModel
from django.utils.translation import gettext_lazy as _
from django.utils.safestring import mark_safe

from tinymce.models import HTMLField
from imagefield.fields import ImageField
# Create your models here.


class Page(TimeStampedModel):
    title = models.CharField(max_length=255, verbose_name=_("Titel"))
    image = ImageField(formats={
        'original': ['default'],
        'site': ['default', ('crop', (1200, 720))],
        'og_image': ['default', ('crop', (1200, 630))],
        'twitter': ['default', ('crop', (800, 418))],
        'square': ['default', ('crop', (1200, 1200))],
    },
                       auto_add_fields=True,
                       blank=True,
                       verbose_name=_("Bild"))
    content = HTMLField(verbose_name=_("Inhalt"))

    active = models.BooleanField(default=True,
                                 verbose_name=_("Aktiviert"),
                                 help_text=_("Wenn die Seite aktiviert ist, können Leute sie sehen."))

    is_rtl = models.BooleanField(
        default=False,
        verbose_name=_("RTL"),
        help_text=_("Aktiviere diese Option, falls der Text von rechts nach links geschrieben wird."))

    def plain_content(self):
        return self.content

    def safe_content(self):
        return mark_safe(self.content)

    owner = models.OneToOneField(
        get_user_model(),
        models.CASCADE,
        related_name="page",
    )
    id = models.UUIDField(default=uuid.uuid4, primary_key=True)

    def get_absolute_url(self):
        return reverse('pages:page_detail', args=(self.id, ))
