from relational_tool.users.tasks import send_success_message
from typing import Optional
import csv

from django.contrib.auth import get_user_model
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import redirect_to_login
from django.core.exceptions import PermissionDenied
from django.db import models
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.db.models.query_utils import Q
from django.forms.models import BaseModelForm
from django.http.request import HttpRequest
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.utils.translation import gettext as _
from sesame.utils import get_parameters

from relational_tool.pages.models import Page
from relational_tool.users.forms import PageSignupForm
from relational_tool.users.views import get_auth_link

# Create your views here.

User = get_user_model()


class PageCreateView(LoginRequiredMixin, UserPassesTestMixin, CreateView):
    model = Page

    fields = ['image', 'title', 'content', 'active', 'is_rtl']

    def test_func(self):
        return not Page.objects.filter(owner=self.request.user).exists()

    def handle_no_permission(self):
        if self.request.user.is_authenticated:
            return redirect("pages:page_update", pk=self.request.user.page.pk)
        return redirect_to_login(self.request.get_full_path(), self.get_login_url(), self.get_redirect_field_name())

    def form_valid(self, form: BaseModelForm) -> HttpResponse:
        form.instance.owner = self.request.user
        return super().form_valid(form)


# /page/:id/ => Need to handle post data
class PageDetailView(DetailView):
    model = Page

    def get_queryset(self) -> models.query.QuerySet:
        if self.request.user.is_authenticated:
            return super().get_queryset().filter(Q(owner__is_active=True, active=True) | Q(owner=self.request.user))
        return super().get_queryset().filter(owner__is_active=True, active=True)

    def post(self, request: HttpRequest, *args, **kwargs):
        self.object = self.get_object()
        form = PageSignupForm(request.POST, page=self.object)

        if form.is_valid():
            form.save()
            user = form.instance
            auth_link = get_auth_link(self.request, user, '/create/')
            send_success_message.delay(user.pk, auth_link)

            return render(request, "pages/signup_success.html", {'form': form})

        context = self.get_context_data(object=self.object, form=form)
        return self.render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'form' not in context:
            context['form'] = PageSignupForm(page=self.object)
        return context


# /page/:id/update/
class SharePageView(PageDetailView):
    model = Page
    template_name = "pages/page_share.html"


class PageUpdateView(UpdateView, LoginRequiredMixin, UserPassesTestMixin):
    model = Page

    fields = ['image', 'image_ppoi', 'title', 'content', 'active', 'is_rtl']

    def test_func(self) -> Optional[bool]:
        return self.get_object().owner == self.request.user or self.request.user.is_superuser


@login_required
def contacts_view(request):
    page = request.user.page
    contacts = User.objects.filter(signed_up_through=page, is_active=True)

    return render(request, 'pages/contact_list.html', {'contacts': contacts})


@login_required
def contacts_csv(request):
    page = request.user.page
    contacts = User.objects.filter(signed_up_through=page, is_active=True)
    response = HttpResponse(content_type='text/csv', )

    response['Content-Disposition'] = f'attachment; filename="{request.user.username}_contacts.csv"'

    writer = csv.writer(response)
    writer.writerow([
        _("first name"),
        _("last name"),
        _("email address"),
        _("Telefon"),
        _("PLZ"),
    ])
    for contact in contacts:
        writer.writerow([
            contact.first_name,
            contact.last_name,
            contact.email,
            contact.phone,
            contact.zip_code,
        ])

    return response
